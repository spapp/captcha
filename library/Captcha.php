<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once('Captcha/Store.php');
require_once('Captcha/Adapter.php');
require_once('Trait/SetOptions.php');
/**
 * Class Captcha
 */
class Captcha {
    /**
     * Inherit from trait.
     */
    use SetOptions;

    /**
     * Default captcha store name.
     */
    const DEFAULT_STORE = 'Session';

    /**
     * Default captcha adapter name.
     */
    const DEFAULT_ADAPTER = 'Image';

    /**
     * Captcha class instance.
     *
     * @var null|Captcha
     * @static
     */
    protected static $instance = null;

    /**
     * @var Captcha_Store_Abstract
     */
    protected $store = null;

    /**
     * @var Captcha_Adapter_Generator_Abstract
     */
    protected $adapter = null;

    /**
     * Constructor
     *
     * @param null|array $options
     */
    public function __construct($options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Checks the user value is valid.
     *
     * @param string $userValue
     *
     * @return bool
     */
    public function isValid($userValue) {
        return $this->getStore()->isValid($userValue);
    }

    /**
     * Returns this captcha as string.
     *
     * @return string
     */
    public function __toString() {
        $this->getStore()->setValue($this->getAdapter()->generate());

        return (string)$this->getAdapter();
    }

    /**
     * Returns captcha instance.
     *
     * @static
     * @return Captcha|null
     */
    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Sets the captcha adapter.
     *
     * @param string|array|Captcha_Adapter_Generator_Abstract $name
     * @param array|null                                      $options
     *
     * @return Captcha
     */
    public function setAdapter($name, $options = null) {
        $this->adapter = Captcha_Adapter::factory($name, $options);

        return $this;
    }

    /**
     * Returns the current captcha adapter.
     *
     * @return Captcha_Adapter_Generator_Abstract|null
     */
    public function getAdapter() {
        if (null === $this->adapter) {
            $this->adapter = Captcha_Adapter::factory(self::DEFAULT_ADAPTER);
        }

        return $this->adapter;
    }

    /**
     * Sets the captcha store.
     *
     * @param string|array|Captcha_Store_Abstract $name
     * @param array|null                          $options
     *
     * @return Captcha
     */
    public function setStore($name, $options = null) {
        $this->store = Captcha_Store::factory($name, $options);

        return $this;
    }

    /**
     * Returns the current captcha store.
     *
     * @return Captcha_Store_Abstract|null
     */
    public function getStore() {
        if (null === $this->store) {
            $this->store = Captcha_Store::factory(self::DEFAULT_STORE);
        }

        return $this->store;
    }
} 