<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
/**
 * Class Captcha_Store
 */
class Captcha_Store {

    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a captcha store.
     *
     * @param string|array|Captcha_Store_Abstract $storeName
     * @param array|null                          $options
     *
     * @return Captcha_Store_Abstract
     * @throws Captcha_Store_Exception
     */
    public static function factory($storeName, $options = null) {
        if ($storeName instanceof Captcha_Store_Abstract) {
            return $storeName;
        }

        if (is_array($storeName)) {
            $options = $storeName['params'];
            $storeName = $storeName['adapter'];
        } elseif (!is_string($storeName)) {
            include_once('Store/Exception.php');
            throw new Captcha_Store_Exception('The storeName must be a string or an array.');
        }

        if (null === $options) {
            $options = array();
        }

        $storeName = ucfirst(trim($storeName));
        $className = __CLASS__ . '_' . $storeName;
        $classPath = 'Store/' . $storeName . '.php';

        include_once($classPath);

        return new $className($options);
    }
} 