<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
/**
 * Class Captcha_Store_Abstract
 */
abstract class Captcha_Store_Abstract {
    /**
     * Inherit from trait.
     */
    use SetOptions;

    /**
     * Constructor
     *
     * @param array $options
     */
    public function __construct(array $options) {
        $this->setOptions($options);
    }

    /**
     * Checks the user value is valid.
     *
     * @param mixed $userValue
     *
     * @return bool
     */
    public final function isValid($userValue) {
        return ((string)$userValue === (string)$this->getValue());
    }

    /**
     * Sets the current value.
     *
     * @param string $value
     *
     * @return mixed
     */
    abstract public function setValue($value);

    /**
     * Returns the current value.
     *
     * @return mixed
     */
    abstract public function getValue();
} 