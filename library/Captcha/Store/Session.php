<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once('Abstract.php');
/**
 * Class Captcha_Store_Session
 */
class Captcha_Store_Session extends Captcha_Store_Abstract {
    /**
     * @var string
     */
    protected $key = 'Captcha_Value';

    /**
     * Constructor
     *
     * @param string|array $options
     *
     * @throws Captcha_Store_Exception
     */
    public function __construct($options = null) {
        session_start();

        if (is_string($options)) {
            $options = array('key' => $options);
        }

        if (is_array($options)) {
            parent::__construct($options);
        }
    }

    /**
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function setKey($key) {
        $this->key = $key;

        return $this;
    }

    /**
     * @param string $value
     *
     * @see Captcha_Store_Abstract
     * @return $this|mixed
     */
    public function setValue($value) {
        $_SESSION[$this->getKey()] = $value;

        return $this;
    }

    /**
     * @see Captcha_Store_Abstract
     * @return mixed
     */
    public function getValue() {
        return $_SESSION[$this->getKey()];
    }
} 