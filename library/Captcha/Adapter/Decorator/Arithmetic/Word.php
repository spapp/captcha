<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */
require_once('Abstract.php');
/**
 * Class Captcha_Adapter_Decorator_Arithmetic_Word
 */
class Captcha_Adapter_Decorator_Arithmetic_Word extends Captcha_Adapter_Decorator_Arithmetic_Abstract {
    /**
     * Numbers as text.
     *
     * @var array
     */
    protected $numbers = array(
        'hu' => array(
            'nulla',
            'egy',
            'kettő',
            'három',
            'négy',
            'öt',
            'hat',
            'hét',
            'nyolc',
            'kilenc'
        ),
        'en' => array(
            'zero',
            'one',
            'two',
            'three',
            'four',
            'five',
            'six',
            'seven',
            'eight',
            'nine'
        )
    );

    /**
     * Arithmetic signs as text.
     *
     * @var array
     */
    protected $signs = array(
        'hu' => array(
            'plus'  => 'meg',
            'minus' => 'minusz',
            'equal' => ', egyenlő'
        ),
        'en' => array(
            'plus'  => 'plus',
            'minus' => 'minus',
            'equal' => 'is equal'
        )
    );

    /**
     * Returns lang specific numbers.
     *
     * If it is not exist then use fallback language.
     *
     * @return array
     */
    protected function getNumbers() {
        $lang = $this->getLanguage();
        if (!array_key_exists($lang, $this->numbers)) {
            $lang = self::FALLBACK_LANGUAGE;
        }

        return $this->numbers[$lang];
    }

    /**
     * Returns lang specific signs.
     *
     * If it is not exist then use fallback language.
     *
     * @return array
     */
    protected function getSigns() {
        $lang = $this->getLanguage();
        if (!array_key_exists($lang, $this->signs)) {
            $lang = self::FALLBACK_LANGUAGE;
        }

        return $this->signs[$lang];
    }

    /**
     * Returns a sign text.
     *
     * @param string $signName
     *
     * @return string
     */
    protected function getSign($signName) {
        $signs = $this->getSigns();

        return $signs[$signName];
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getPlusSign() {
        return $this->getSign('plus');
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getMinusSign() {
        return $this->getSign('minus');
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getEqualSign() {
        return $this->getSign('equal');
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstract
     *
     * @param int $number
     *
     * @return int|mixed
     */
    protected function getNumber($number) {
        $numbers = $this->getNumbers();

        return $numbers[$number];
    }
} 