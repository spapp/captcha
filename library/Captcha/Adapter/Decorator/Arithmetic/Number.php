<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */
require_once('Abstract.php');
/**
 * Class Captcha_Adapter_Decorator_Arithmetic_Number
 */
class Captcha_Adapter_Decorator_Arithmetic_Number extends Captcha_Adapter_Decorator_Arithmetic_Abstract {
    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getPlusSign() {
        return self::PLUS_SIGN;
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getMinusSign() {
        return self::MINUS_SIGN;
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstrac
     * @return string
     */
    protected function getEqualSign() {
        return self::EQUAL_SIGN;
    }

    /**
     * @see Captcha_Adapter_Decorator_Arithmetic_Abstract
     *
     * @param int $number
     *
     * @return int|mixed
     */
    protected function getNumber($number) {
        return $number;
    }
} 