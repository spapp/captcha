<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */
require_once(dirname(dirname(__FILE__)) . '/Interface.php');
/**
 * Class Captcha_Adapter_Decorator_Arithmetic_Abstract
 */
abstract class Captcha_Adapter_Decorator_Arithmetic_Abstract implements Captcha_Adapter_Decorator_Interface {
    /**
     * Fallback language.
     */
    const FALLBACK_LANGUAGE = 'en';

    /**
     * Plus sign.
     */
    const PLUS_SIGN = '+';

    /**
     * Minus sign.
     */
    const MINUS_SIGN = '-';

    /**
     * Equals sign.
     */
    const EQUAL_SIGN = '=';

    /**
     * Maximum value.
     *
     * @var int
     */
    protected $maxValue = null;

    /**
     * Minimum value.
     *
     * @var int
     */
    protected $minValue = null;

    /**
     * Current language.
     *
     * @var string
     */
    protected $language = null;

    /**
     * Sets maximum value.
     *
     * @param int $maxValue
     *
     * @return $this
     */
    public function setMaxValue($maxValue) {
        $this->maxValue = (int)$maxValue;

        return $this;
    }

    /**
     * Returns maximum value.
     *
     * @return int
     */
    public function getMaxValue() {
        return $this->maxValue;
    }

    /**
     * Sets minimum value.
     *
     * @param int $minValue
     *
     * @return $this
     */
    public function setMinValue($minValue) {
        $this->minValue = (int)$minValue;

        return $this;
    }

    /**
     * Returns minimum value.
     *
     * @return int
     */
    public function getMinValue() {
        return $this->minValue;
    }

    /**
     * Sets language.
     *
     * @param string $language
     *
     * @return $this
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Returns current language.
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Decorates an arithmetic captcha text.
     *
     * Return pattern:
     * <code>
     *      ^[0-9]+ [+-] [0-9]+ =$  // with Number decorator
     *
     *      OR
     *
     *      ^.+$    // with Word decorator
     * </code>
     *
     * @see Captcha_Adapter_Decorator_Interface
     *
     * @param int $text
     *
     * @return string
     */
    public final function decorate($text) {
        $text = intval($text);
        $a = mt_rand($this->getMinValue(), $this->getMaxValue());
        $return = array($this->getNumber($a));

        if ($text > $a) {
            array_push($return, $this->getPlusSign(), $this->getNumber($text - $a));
        } elseif ($text < $a) {
            array_push($return, $this->getMinusSign(), $this->getNumber($a - $text));
        } else {
            array_push($return,
                ((mt_rand(1, 2) % 2) ? $this->getPlusSign() : $this->getMinusSign()),
                       $this->getNumber(0));
        }

        array_push($return, $this->getEqualSign());

        $return = implode(' ', $return);
        $encoding = $this->getEncoding($return);

        return (mb_strtoupper(mb_substr($return, 0, 1, $encoding), $encoding) . mb_substr($return, 1, null, $encoding));
    }

    /**
     * Returns string encoding.
     *
     * @param string $text
     *
     * @return string
     */
    protected function getEncoding($text) {
        return mb_detect_encoding($text);
    }

    /**
     * Returns plus sign or text.
     *
     * @return string
     */
    abstract protected function getPlusSign();

    /**
     * Returns minus sign or text.
     *
     * @return string
     */
    abstract protected function getMinusSign();

    /**
     * Returns equals sign or text.
     *
     * @return string
     */
    abstract protected function getEqualSign();

    /**
     * Returns number or text.
     *
     * @param int $number
     *
     * @return mixed
     */
    abstract protected function getNumber($number);
} 