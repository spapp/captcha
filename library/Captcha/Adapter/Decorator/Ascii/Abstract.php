<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once(dirname(dirname(__FILE__)) . '/Interface.php');
/**
 * Class Captcha_Adapter_Decorator_Ascii_Abstract
 */
abstract class Captcha_Adapter_Decorator_Ascii_Abstract implements Captcha_Adapter_Decorator_Interface {
    /**
     * @var array
     */
    protected $characters = array();

    /**
     * Constructor
     */
    public function __construct() {

    }

    /**
     * Decorate a string.
     *
     * @param string $text
     *
     * @return string
     */
    public final function decorate($text) {
        $text = str_split($text);
        $return = array();

        foreach ($text as &$char) {
            $char = $this->getChar($char);

            for ($i = 0; $i < count($char); $i++) {
                if (!isset($return[$i])) {
                    $return[$i] = array();
                }
                array_push($return[$i], $char[$i]);
            }
        }

        foreach ($return as &$item) {
            $item = implode(' ', $item);
        }

        return implode(PHP_EOL, $return);
    }

    /**
     * @param string $char
     *
     * @return array
     */
    public final function getChar($char) {
        if (array_key_exists($char, $this->characters)) {
            $char = $this->characters[$char];
        }

        return (array)$char;
    }
} 