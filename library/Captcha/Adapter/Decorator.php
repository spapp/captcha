<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.07.
 */
/**
 * Class Captcha_Adapter_Decorator
 */
class Captcha_Adapter_Decorator {
    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a captcha ascii decorator.
     *
     * @param string $decoratorName
     *
     * @return Captcha_Adapter_Decorator_Ascii_Abstract
     */
    public static function factory($decoratorName) {
        $decoratorName = ucfirst(trim($decoratorName));
        $className = __CLASS__ . '_' . $decoratorName;
        $classPath = 'Decorator/' . str_replace('_', '/', $decoratorName) . '.php';

        include_once($classPath);

        return new $className();
    }
} 