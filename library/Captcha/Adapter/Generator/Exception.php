<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.11.
 */
/**
 * Class Captcha_Adapter_Generator_Exception
 */
class Captcha_Adapter_Generator_Exception extends Exception {

}