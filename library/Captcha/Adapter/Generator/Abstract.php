<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once(dirname(dirname(__FILE__)) . '/Decorator.php');
require_once('Interface.php');
/**
 * Class Captcha_Adapter_Generator_Abstract
 */
abstract class Captcha_Adapter_Generator_Abstract implements Captcha_Adapter_Generator_Interface {
    /**
     * Inherit from trait.
     */
    use SetOptions;

    /**
     * Decorator class prefix.
     *
     * @var string
     */
    protected $decoratorNamePrefix = '';

    /**
     * Default decorator name.
     *
     * @var string
     */
    protected $defaultDecoratorName = '';

    /**
     * @var string
     */
    protected $captchaValue = null;

    /**
     * @var bool
     */
    protected $xhtml = false;

    /**
     * @var Captcha_Adapter_Decorator_Interface
     */
    protected $decorator = null;

    /**
     * Constructor
     *
     * @param array $options
     */
    public function __construct(array $options) {
        $this->setOptions($options);
    }

    /**
     * Returns true if the html doctype is xhtml.
     *
     * @return bool
     */
    public function isXhtml() {
        return $this->xhtml;
    }

    /**
     * Sets the doctype is xhtml or not.
     *
     * @param bool $isXhtml
     *
     * @return $this
     */
    public function setXhtml($isXhtml) {
        $this->xhtml = (bool)$isXhtml;

        return $this;
    }

    /**
     * Sets the captcha value.
     *
     * @param string $captchaValue
     *
     * @return $this
     */
    public function setCaptchaValue($captchaValue) {
        $this->captchaValue = $captchaValue;

        return $this;
    }

    /**
     * Returns the current captcha value.
     *
     * @return string|null
     */
    public function getCaptchaValue() {
        return $this->captchaValue;
    }

    /**
     * Generates a new captcha string.
     *
     * @see generateCaptcha
     * @return string
     */
    public function generate() {
        $this->setCaptchaValue($this->generateCaptcha());

        return $this->getCaptchaValue();
    }

    /**
     * Sets the ascii decorator.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setDecorator($name) {
        $this->decorator = Captcha_Adapter_Decorator::factory($this->decoratorNamePrefix . ucfirst($name));

        return $this;
    }

    /**
     * Returns the ascii decorator.
     *
     * @return Captcha_Adapter_Decorator_Interface
     */
    public function getDecorator() {
        if (null === $this->decorator) {
            $this->decorator = Captcha_Adapter_Decorator::factory($this->defaultDecoratorName);
        }

        return $this->decorator;
    }

    /**
     * Returns a html input field.
     *
     * @param string $name
     * @param string $cssClass
     *
     * @return string
     */
    protected function getHtmlInput($name = 'captcha', $cssClass = 'captchaField') {
        $input = '<input type="text" value="" class="' . $cssClass . '"name="' . $name . '"';

        return $input . $this->getTagCloseSign();
    }

    /**
     * Returns html tag close sign.
     *
     * @return string
     */
    protected function getTagCloseSign() {
        if ($this->isXhtml()) {
            return '/>';
        }

        return '>';
    }

    /**
     * Generates a new captcha string.
     *
     * @return string
     */
    abstract protected function generateCaptcha();
} 