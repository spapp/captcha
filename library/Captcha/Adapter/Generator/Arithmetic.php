<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.11.
 */
require_once('Abstract.php');
/**
 * Class Arithmetic
 */
class Captcha_Adapter_Generator_Arithmetic extends Captcha_Adapter_Generator_Abstract {
    /**
     * Default maximum value.
     */
    const DEFAULT_MAX_VALUE = 9;

    /**
     * Default minimum value.
     */
    const DEFAULT_MIN_VALUE = 0;

    /**
     * Maximum value.
     *
     * @var int
     */
    protected $maxValue = self::DEFAULT_MAX_VALUE;

    /**
     * Minimum value.
     *
     * @var int
     */
    protected $minValue = self::DEFAULT_MIN_VALUE;

    /**
     * Sets maximum value.
     *
     * @param int $maxValue
     *
     * @return $this
     */
    public function setMaxValue($maxValue) {
        $this->maxValue = (int)$maxValue;

        return $this;
    }

    /**
     * Returns maximum value.
     *
     * @return int
     */
    public function getMaxValue() {
        return $this->maxValue;
    }

    /**
     * Sets minimum value.
     *
     * @param int $minValue
     *
     * @return $this
     */
    public function setMinValue($minValue) {
        $this->minValue = (int)$minValue;

        return $this;
    }

    /**
     * Returns minimum value.
     *
     * @return int
     */
    public function getMinValue() {
        return $this->minValue;
    }

    /**
     * Generates a new captcha string.
     *
     * @see Captcha_Adapter_Generator_Abstract
     * @return string
     */
    protected function generateCaptcha() {
        return (int)mt_rand($this->getMinValue(), $this->getMaxValue());
    }
}