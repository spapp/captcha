<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.11.
 */
require_once('Abstract.php');
/**
 * Class Word
 */
class Captcha_Adapter_Generator_Word extends Captcha_Adapter_Generator_Abstract {
    /**
     * Inherit from trait.
     */
    use SetOptions;

    /**
     * Default captcha length.
     */
    const DEFAULT_LENGTH = 4;

    /**
     * Letters.
     */
    const LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * Numbers.
     */
    const NUMBERS = '0123456789';

    /**
     * If it's TRUE then it will use lowercase letters.
     *
     * @var bool
     */
    protected $useLowerCase = false;

    /**
     * If it's TRUE then it will use uppercase letters.
     *
     * @var bool
     */
    protected $useUpperCase = true;

    /**
     * If it's TRUE then it will use numbers.
     *
     * @var bool
     */
    protected $useNumbers = true;

    /**
     * Captcha length.
     *
     * @var int
     */
    protected $length = self::DEFAULT_LENGTH;

    /**
     * Returns captcha character set.
     *
     * @return string
     * @throws Captcha_Adapter_Generator_Exception
     */
    public function getCharacters() {
        $characters = '';

        if (true === $this->useLowerCase) {
            $characters .= strtolower(self::LETTERS);
        }

        if (true === $this->useUpperCase) {
            $characters .= strtoupper(self::LETTERS);
        }

        if (true === $this->useNumbers) {
            $characters .= self::NUMBERS;
        }

        if (strlen($characters) < 1) {
            include_once('Exception.php');
            throw new Captcha_Adapter_Generator_Exception('No captcha character set.');
        }

        return $characters;
    }

    /**
     * If sets TRUE then it will use lowercase letters otherwise not.
     *
     * @param bool $useLowerCase
     *
     * @return $this
     */
    public function setUseLowerCase($useLowerCase) {
        $this->useLowerCase = (bool)$useLowerCase;

        return $this;
    }

    /**
     * If sets TRUE then it will use uppercase letters otherwise not.
     *
     * @param bool $useUpperCase
     *
     * @return $this
     */
    public function setUseUpperCase($useUpperCase) {
        $this->useUpperCase = (bool)$useUpperCase;

        return $this;
    }

    /**
     * If sets TRUE then it will use numbers otherwise not.
     *
     * @param bool $useNumbers
     *
     * @return $this
     */
    public function setUseNumbers($useNumbers) {
        $this->useNumbers = (bool)$useNumbers;

        return $this;
    }

    /**
     * Sets captcha length.
     *
     * @param int $length
     *
     * @return $this
     */
    public function setLength($length) {
        $this->length = (int)$length;

        return $this;
    }

    /**
     * Returns captcha length.
     *
     * @return int
     */
    public function getLength() {
        return $this->length;
    }

    /**
     * Generates a new captcha string.
     *
     * @return string
     */
    protected function generateCaptcha() {
        $captcha = '';
        $captchaLength = $this->getLength();
        $characters = str_split($this->getCharacters());

        while ($captchaLength) {
            shuffle($characters);
            $captcha .= $characters[array_rand($characters)];
            --$captchaLength;
        }

        return $captcha;
    }
} 