<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */
/**
 * Interface Captcha_Adapter_Interface
 */
interface Captcha_Adapter_Interface {
    /**
     * Returns captcha as a html.
     *
     * @return string
     */
    public function toHtml();
    /**
     * Returns this as string.
     *
     * @return string
     */
    public function __toString();
} 