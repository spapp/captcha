<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once('Generator/Arithmetic.php');
require_once('Decorator.php');
require_once('Interface.php');
/**
 * Class Captcha_Adapter_Arithmetic
 */
class Captcha_Adapter_Arithmetic extends Captcha_Adapter_Generator_Arithmetic implements Captcha_Adapter_Interface {
    /**
     * @const string
     */
    const LANGUAGE_EN = 'en';

    /**
     * @const string
     */
    const LANGUAGE_HU = 'hu';

    /**
     * @const string
     */
    const DEFAULT_LANGUAGE = self::LANGUAGE_EN;

    /**
     * @see Captcha_Adapter_Generator_Abstract
     * @var string
     */
    protected $decoratorNamePrefix = 'Arithmetic_';

    /**
     * @see Captcha_Adapter_Generator_Abstract
     * @var string
     */
    protected $defaultDecoratorName = 'Arithmetic_Number';

    /**
     * Language for Arithmetic_Word
     *
     * @var string
     */
    protected $language = self::DEFAULT_LANGUAGE;

    /**
     * Sets language.
     *
     * @param string $language
     *
     * @return $this
     */
    public function setLanguage($language) {
        $this->language = $language;

        return $this;
    }

    /**
     * Returns current language.
     *
     * @return string
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @see Captcha_Adapter_Interface
     * @return string
     */
    public function toHtml() {
        $this->getDecorator()->setMaxValue($this->getMaxValue());
        $this->getDecorator()->setMinValue($this->getMinValue());
        $this->getDecorator()->setLanguage($this->getLanguage());

        $html = array(
            '<span>',
            $this->getDecorator()->decorate($this->getCaptchaValue()),
            '</span>',

        );

        array_push($html, $this->getHtmlInput());

        return implode(PHP_EOL, $html);
    }

    /**
     * @see Captcha_Adapter_Interface
     * @return string
     */
    public function __toString() {
        return (string)$this->toHtml();
    }
} 