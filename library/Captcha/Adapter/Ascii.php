<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once('Generator/Word.php');
require_once('Interface.php');
/**
 * Class Captcha_Adapter_Ascii
 */
class Captcha_Adapter_Ascii extends Captcha_Adapter_Generator_Word implements Captcha_Adapter_Interface {
    /**
     * @see Captcha_Adapter_Generator_Abstract
     * @var string
     */
    protected $decoratorNamePrefix = 'Ascii_';

    /**
     * @see Captcha_Adapter_Generator_Abstract
     * @var string
     */
    protected $defaultDecoratorName = 'Ascii_Small';

    /**
     * @see Captcha_Adapter_Interface
     * @return string
     */
    public function toHtml() {
        $html = array(
            '<pre>',
            $this->getDecorator()->decorate($this->getCaptchaValue()),
            '</pre>'
        );

        array_push($html, $this->getHtmlInput());

        return implode(PHP_EOL, $html);
    }

    /**
     * @see Captcha_Adapter_Interface
     * @return string
     */
    public function __toString() {
        return (string)$this->toHtml();
    }
} 