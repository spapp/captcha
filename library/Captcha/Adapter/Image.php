<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
require_once('Generator/Word.php');
require_once('Interface.php');
/**
 * Class Captcha_Adapter_Image
 */
class Captcha_Adapter_Image extends Captcha_Adapter_Generator_Word implements Captcha_Adapter_Interface {
    /**
     * Internet media type of PNG image
     */
    const MIME_IMAGE_PNG = 'image/png';

    /**
     * Internet media type of JPEG image
     */
    const MIME_IMAGE_JPEG = 'image/jpeg';

    /**
     * Internet media type of GIF image
     */
    const MIME_IMAGE_GIF = 'image/gif';

    /**
     * Image default width.
     */
    const DEFAULT_IMAGE_WIDTH = 120;

    /**
     * Image default height.
     */
    const DEFAULT_IMAGE_HEIGHT = 50;

    /**
     * Image default mime type.
     */
    const DEFAULT_IMAGE_MIME_TYPE = self::MIME_IMAGE_PNG;

    /**
     * Image width.
     *
     * @var int
     */
    protected $width = self::DEFAULT_IMAGE_WIDTH;

    /**
     * Image height.
     *
     * @var int
     */
    protected $height = self::DEFAULT_IMAGE_HEIGHT;

    /**
     * Image mime type.
     *
     * @var string
     */
    protected $mimeType = self::DEFAULT_IMAGE_MIME_TYPE;

    /**
     * Image alt text.
     *
     * @var string
     */
    protected $alt = '';

    /**
     * @see Captcha_Adapter_Generator_Word::toHtml
     * @return string
     */
    public function toHtml() {
        $html = array(
            '<img width="' . $this->getWidth() .
            '" height="' . $this->getHeight() .
            '" alt="' . $this->getAlt() .
            '" src="' . $this->getSrc() . '"' .
            $this->getTagCloseSign(),
            '<br>'
        );

        array_push($html, $this->getHtmlInput());

        return implode(PHP_EOL, $html);
    }

    /**
     * @see Captcha_Adapter_Interface
     * @return string
     */
    public function __toString() {
        return (string)$this->toHtml();
    }

    /**
     * Returns image alt text.
     *
     * @return string
     */
    public function getAlt() {
        return $this->alt;
    }

    /**
     * Sets image alt text.
     *
     * @param string $text
     *
     * @return $this
     */
    public function setAlt($text) {
        $this->alt = $text;

        return $this;
    }

    /**
     * Returns image width.
     *
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * Sets image width.
     *
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width) {
        $this->width = (int)$width;

        return $this;
    }

    /**
     * Returns image height.
     *
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * Sets image height.
     *
     * @param int $height
     *
     * @return $this
     */
    public function setHeight($height) {
        $this->height = (int)$height;

        return $this;
    }

    /**
     * Returns image mime type.
     *
     * @return string
     */
    public function getMime() {
        return $this->mimeType;
    }

    /**
     * Sets image mime type.
     *
     * @param string $mimeType
     *
     * @return $this
     */
    public function setMime($mimeType) {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Returns image data as base64 encoded string.
     *
     * @todo refactor -> lot of lines
     * @todo save image to folder; type=data|file ; path, link
     * @todo use ttf font
     * @todo move to Generator_Image
     * @return string
     */
    protected function getImage() {
        $img = imagecreate($this->getWidth(), $this->getHeight());

        imagecolorallocate($img, 0, 255, 255);
        $ellipseColor = imagecolorallocate($img, 0, 180, 255);
        $textColor = imagecolorallocate($img, 0, 0, 255);
        $text = str_split($this->getCaptchaValue());

        $count = rand(25, 50);

        for ($i = 0; $i < $count; $i++) {
            imageellipse($img,
                         rand(5, $this->getWidth()),
                         rand(5, $this->getHeight()),
                         rand(10, 100),
                         rand(50, 250),
                         $ellipseColor);
        }

        $x = 20;
        foreach ($text as $char) {
            imagestring($img, 5, $x, rand(10, $this->getHeight() - 25), $char, $textColor);
            $x = $x + 20;
        }

        ob_start();

        switch ($this->getMime()) {
            case self::MIME_IMAGE_GIF:
                imagegif($img);
                break;
            case self::MIME_IMAGE_JPEG:
                imagejpeg($img);
                break;
            case self::MIME_IMAGE_PNG:
                imagepng($img);
                break;
        }

        $imgData = ob_get_contents();

        ob_end_clean();

        imagedestroy($img);

        return base64_encode($imgData);
    }

    /**
     * Returns image tag src value.
     *
     * @return string
     */
    protected function getSrc() {
        $src = 'data:' . $this->getMime() . ';base64,' . $this->getImage();

        return $src;
    }
} 