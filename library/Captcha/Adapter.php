<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.06.
 */
/**
 * Class Captcha_Adapter
 */
class Captcha_Adapter {

    /**
     * Constructor
     */
    protected function __construct() {

    }

    /**
     * Returns a Captcha_Adapter.
     *
     * @param string|array|Captcha_Adapter_Interface $adapterName
     * @param array|null                            $options
     *
     * @return Captcha_Adapter_Interface
     * @throws Captcha_Adapter_Exception
     */
    public static function factory($adapterName, $options = null) {
        if ($adapterName instanceof Captcha_Adapter_Interface) {
            return $adapterName;
        }

        if (is_array($adapterName)) {
            $options = $adapterName['params'];
            $adapterName = $adapterName['adapter'];
        } elseif (!is_string($adapterName)) {
            include_once('Adapter/Exception.php');
            throw new Captcha_Adapter_Exception('The adapterName must be a string or an array.');
        }

        if (null === $options) {
            $options = array();
        }

        $adapterName = ucfirst(trim($adapterName));
        $className = __CLASS__ . '_' . $adapterName;
        $classPath = 'Adapter/' . $adapterName . '.php';

        include_once($classPath);

        return new $className($options);
    }
}
