<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.08.
 */
/**
 * Trait SetOptions
 */
trait SetOptions {
    /**
     * Sets some options.
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options) {
        foreach ($options as $name => $value) {
            $method = 'set' . ucfirst($name);
            if (method_exists($this, $method)) {
                call_user_func_array(array(
                                         $this,
                                         $method
                                     ),
                                     (array)$value);
            }
        }

        return $this;
    }
} 