<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.07.
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

header('Content-Type: text/html; charset=utf-8');

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require_once(APPLICATION_PATH . '/library/Captcha.php');

$ok = '-';

if (!isset($options) or !is_array($options)) {
    $options = array();
}

Captcha::getInstance()->setOptions($options);

if ('POST' === $_SERVER['REQUEST_METHOD']) {
    if (Captcha::getInstance()->isValid($_POST['captcha'])) {
        $ok = 'OK';
    } else {
        $ok = 'FAIL';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Captcha test</title>

    <style>
        form {
            text-align: center;
        }
    </style>
</head>
<body>
    <form method="post">
        <h1>Captcha test</h1>
        <h5><?php echo $ok; ?></h5>

        <div class="captcha"><?php echo Captcha::getInstance(); ?></div>
        <input type="submit" value="Test">
    </form>

<pre>
SESSION

<?php print_r($_SESSION); ?>
</pre>
</body>
</html>
