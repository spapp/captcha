<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */

$options = array(
    'adapter' => array(
        'adapter' => 'ascii',
        'params'  => array(
            'decorator'    => 'small',
            'useLowerCase' => true,
            'useUpperCase' => true,
            'useNumbers'   => true
        )
    )
);

require_once('captcha.php');