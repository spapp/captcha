<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.12.
 */

$options = array(
    'adapter' => array(
        'adapter' => 'arithmetic',
        'params'  => array(
            'decorator'    => 'word',
            'language' => 'hu'
        )
    )
);

require_once('captcha.php');