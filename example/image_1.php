<?php
/**
 * @author    spapp
 * @copyright 2013
 * @license   http://opensource.org/licenses/GPL-3.0 GNU General Public License v3
 * @package   captcha
 * @version   1.0.0
 * @since     2013.11.08.
 */
$options = array(
    'adapter' => array(
        'adapter' => 'image',
        'params'  => array(
            'width'  => 150,
            'height' => 30,
            'mime'   => 'image/gif',
            'length' => 6
        )
    )
);

require_once('captcha.php');
